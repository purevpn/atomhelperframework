require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
   gem 'plist'
   gem 'xcodeproj'
end

puts 'Gems installed and loaded!'

# GET Application Bundle Identifier 
puts "Application Bundle Identifier:"
app_bundle_identifier = gets
app_bundle_identifier = app_bundle_identifier.chomp.strip
  
# Get New Bundle Identifier for HelperTOOL
puts "Bundle Identifier for HelperTool (Ex: com.company.helpertool):"
helpertool_bundle_identifier = gets
helpertool_bundle_identifier = helpertool_bundle_identifier.chomp.strip  


# Update CodeSignUpdate.sh File with New Helper Identifier and App identifier
new_content = ""

File.foreach('Scripts/CodeSignUpdate.sh').with_index do |line, line_num|
   
   if line.include? "bundleIdentifierApplication="
   	   line = "bundleIdentifierApplication=\"#{app_bundle_identifier}\"\n"
   end

   if line.include? "bundleIdentifierHelper="
   	   line = "bundleIdentifierHelper=\"#{helpertool_bundle_identifier}\"\n"
   end

   new_content += line
end

File.open("Scripts/CodeSignUpdate.sh", "w") { |f| f.write new_content }


# Update Constant File
constant_file_content = ""

File.foreach('Source/AtomHelper/HelperConstants.swift').with_index do |line, line_num|
   
   if line.include? "machServiceName"
   	   line = "static var machServiceName = \"#{helpertool_bundle_identifier}\"\n"
   end

   constant_file_content += line
end

File.open("Source/AtomHelper/HelperConstants.swift", "w") { |f| f.write constant_file_content }


proj_path = 'AtomHelperFramework.xcodeproj'

# Open Xcode Project

project = Xcodeproj::Project.open(proj_path)
puts project

# Change Target Name
project.targets.each do |target|
  puts target.name
  if (target.name == "com.atom.helpertool")
  		puts "#{target.name} changing it to #{helpertool_bundle_identifier}"
		target.name = helpertool_bundle_identifier
	end
end


# Load Helper-Info.plist
helper_info_plist_path = 'Source/AtomHelperPlist/Helper-Info.plist'
helper_info_plist = Plist.parse_xml(helper_info_plist_path)

original_helper_bundleIdentifier = helper_info_plist["CFBundleIdentifier"]
original_helper_bundleName = helper_info_plist["CFBundleName"]


helper_info_plist["CFBundleIdentifier"] = helpertool_bundle_identifier
helper_info_plist["CFBundleName"] = helpertool_bundle_identifier

helper_info_plist.save_plist(helper_info_plist_path)


# Load Helper-Launchd.plist
helper_Launched_info_plist_path = 'Source/AtomHelperPlist/Helper-Launchd.plist'
helper_Launched_info_plist = Plist.parse_xml(helper_Launched_info_plist_path)

helper_Launched_Label = helper_Launched_info_plist["Label"]
helper_Launched_machservice = helper_Launched_info_plist["MachServices"]


helper_Launched_info_plist["Label"] = helpertool_bundle_identifier
helper_Launched_info_plist["MachServices"] = {helpertool_bundle_identifier => true}

helper_Launched_info_plist.save_plist(helper_Launched_info_plist_path)

project.save()

puts "Installation completed!"


