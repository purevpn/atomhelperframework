//
//  AppProtocol.swift
//  AtomHelperFramework
//
//  Copyright © 2020 Atom. All rights reserved.
//
//  This file is part of Atom by Secure.
//

import Foundation

@objc(AppProtocol)
public protocol AppProtocol {
    func log(stdOut: String) -> Void
    func log(stdErr: String) -> Void
}
