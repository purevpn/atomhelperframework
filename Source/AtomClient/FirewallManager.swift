//
//  FirewallManager.swift
//
//  Copyright © 2020 Atom. All rights reserved.
//
//  This file is part of Atom by Secure.

import Cocoa
import ServiceManagement

enum FirewallManagerError: Error {
    case fileSystem
}

protocol FirewallManagerFactory {
    func makeFirewallManager() -> FirewallManager
}

public class FirewallManager {
    
    public static let shared = FirewallManager()
    
    private init() {
        
        do {
            
            try HelperAuthorization.authorizationRightsUpdateDatabase()
        } catch {
            //Log.D("Failed to update the authorization database rights with error: \(error)", level: .error)
            log(stdErr: "Failed to update the authorization database rights with error: \(error)")
        }
    }
    
    private var currentHelperConnection: NSXPCConnection?
    @objc dynamic var currentHelperAuthData: NSData?
    
    // Since the interface(s) can change, should only rely on this when changing from one server to a another without explicitly disconnecting
    private var lastVpnServerIp: String?
    
    private var inactiveFirewallTimer: Timer?
    
    private var helperInstallInProgress = false
        
    
    // MARK: - Public functions
    
    public func unloadAndInstallHelper() {
        if let helper = self.helper({ _ in
            self.attemptToInstallHelper()
        }) {
          
            let argumentsArray = ["unloadAndInstallHelper"]
            helper.connectWithPaths(pathsData: getDataAug(array: argumentsArray), withArguments: getDataAug(array: argumentsArray), completion:{ [weak self] (exitCodeArray) in
                self?.log(stdErr: "disableFirewall exit code: \(exitCodeArray[0])")
                self?.attemptToInstallHelper()
            })
            
        } else {
            self.attemptToInstallHelper()
        }
    }

    public func helperInstallStatus(completion: @escaping (_ installed: Bool) -> Void) {
        // Compare the CFBundleShortVersionString from the Info.plist in the helper inside our application bundle with the one on disk
        let helperURL = Bundle.main.bundleURL.appendingPathComponent("Contents/Library/LaunchServices/" + HelperConstants.machServiceName)
        guard
            let helperBundleInfo = CFBundleCopyInfoDictionaryForURL(helperURL as CFURL) as? [String: Any],
            let helperVersion = helperBundleInfo["CFBundleShortVersionString"] as? String,
            let helper = self.helper(completion) else {
                completion(false)
                return
        }
        
//        helper.getVersion { installedHelperVersion in
//            completion(installedHelperVersion == helperVersion)
//        }
        
        let argumentsArray = ["getVersion"]
        helper.connectWithPaths(pathsData: getDataAug(array: argumentsArray), withArguments: getDataAug(array: argumentsArray), completion:{ [weak self] (exitCodeArray) in
            self?.log(stdErr: "disableFirewall exit code: \(exitCodeArray[0])")
            completion(exitCodeArray[0] as! String == helperVersion)
        })
    }

    public func destroyCachedAuthorization() {
         self.currentHelperAuthData = nil
     }
     
     public func helperInstall() throws -> Bool {
         
         // Install and activate the helper inside our application bundle to disk.
         
         var cfError: Unmanaged<CFError>?
         var authItem = AuthorizationItem(name: (kSMRightBlessPrivilegedHelper as NSString).utf8String!, valueLength: 0, value:UnsafeMutableRawPointer(bitPattern: 0), flags: 0)
         var authRights = withUnsafeMutablePointer(to: &authItem) { pointer in
             AuthorizationRights(count: 1, items: pointer)
         }
         guard
             let authRef = try HelperAuthorization.authorizationRef(&authRights, nil, [.interactionAllowed, .extendRights, .preAuthorize]),
             SMJobBless(kSMDomainSystemLaunchd, HelperConstants.machServiceName as CFString, authRef, &cfError) else {
                 if let error = cfError?.takeRetainedValue() { throw error }
                 return false
         }
         
         self.currentHelperConnection?.invalidate()
         self.currentHelperConnection = nil
         
         return true
     }
    
    // MARK: - Private functions
    func installHelperIfNeeded() {
        guard !helperInstallInProgress else { return }
        
        helperInstallInProgress = true
        
        helperInstallStatus { [unowned self] (installed) in
            if installed {
                self.helperSuccessfullyInstalled()
                self.helperInstallInProgress = false
                
            } else {
                let unloadAndInstallClosure = { [unowned self] in self.unloadAndInstallHelper() }
                unloadAndInstallClosure()
            }
        }
    }
    
    
    private func attemptToInstallHelper() {
        
        do {
            try self.installHelper()
            log(stdErr:"NetworkHelper successfully installed")
            helperSuccessfullyInstalled()
            
        } catch {
            log(stdErr: "NetworkHelper failed to install with error: \(error)")
            self.installHelperIfNeeded()
        }
        
        self.helperInstallInProgress = false
    }
    
    private func helperSuccessfullyInstalled() {
        //            self.isAnyFirewallEnabled { (enabled) in
        //                 print(enabled)
        //        }
    }
    
    // MARK: Helper Connection Methods
    private func helperConnection() -> NSXPCConnection? {
        guard currentHelperConnection == nil else { return currentHelperConnection }
        
        let connection = NSXPCConnection(machServiceName: HelperConstants.machServiceName, options: .privileged)
        connection.exportedInterface = NSXPCInterface(with: AppProtocol.self)
        connection.exportedObject = self
        connection.remoteObjectInterface = NSXPCInterface(with: HelperProtocol.self)
        connection.invalidationHandler = { [unowned self] in
            self.currentHelperConnection?.invalidationHandler = nil
            OperationQueue.main.addOperation {
                self.currentHelperConnection = nil
                //self.installHelperIfNeeded()
            }
        }
        
        currentHelperConnection = connection
        currentHelperConnection?.resume()
        
        return currentHelperConnection
    }
    
    func helper(_ completion: ((Bool) -> Void)? = nil) -> HelperProtocol? {
        // Get the current helper connection and return the remote object (NetworkHelper.swift) as a proxy object to call functions on
        
        guard let helper = self.helperConnection()?.remoteObjectProxyWithErrorHandler({ [unowned self] error in
            //Log.D("Helper connection was closed with error: \(error)", level: .error)
            self.log(stdErr: "Helper connection was closed with error: \(error)")
            if let onCompletion = completion { onCompletion(false) }
            //self.installHelperIfNeeded()
        }) as? HelperProtocol else { return nil }
        return helper
    }
    
    
    
    private func installHelper() throws {
        // Install and activate the helper inside our application bundle to disk
        var cfError: Unmanaged<CFError>?
        var authItem = AuthorizationItem(name: (kSMRightBlessPrivilegedHelper as NSString).utf8String!, valueLength: 0, value: UnsafeMutableRawPointer(bitPattern: 0), flags: 0)
        var authRights = withUnsafeMutablePointer(to: &authItem) { pointer in
            AuthorizationRights(count: 1, items: pointer)
        }
        
        self.currentHelperConnection?.invalidate()
        self.currentHelperConnection = nil
        
        guard let authRef = try HelperAuthorization.authorizationRef(&authRights, nil, [.interactionAllowed, .extendRights, .preAuthorize]),
            SMJobBless(kSMDomainSystemLaunchd, HelperConstants.machServiceName as CFString, authRef, &cfError) else {
                throw cfError?.takeRetainedValue() ?? AppError(message: "unknown failure reason") //NSError(domain: "authRef", code: -1, localizedDescription: "unknown failure reason")
        }
    }

    
}

// MARK: - Firewall enable/disable functions

public extension FirewallManager {
    
    func enableFirewall(ipAddress: String = "", completion: @escaping (Bool) -> ())  {
        do {
            lastVpnServerIp  = ipAddress
            try attemptEnablingFirewall(ipAddress: ipAddress, completionHandler: completion)
        } catch {
            log(stdErr: error.localizedDescription)
            showErrorAlert(error: error)
            //invalidateFirewallTimer()
        }
    }
    
    func disableFirewall(completion: @escaping (Bool) -> ()) {
        inactiveFirewallTimer?.invalidate()
        inactiveFirewallTimer = nil
        
        guard let helper = self.helper() else {
            log(stdErr: "Can not retrieve network helper")
            return
        }

        let argumentsArray = ["disableFirewall"]
        helper.connectWithPaths(pathsData: getDataAug(array: argumentsArray), withArguments: getDataAug(array: argumentsArray), completion:{ [weak self] (exitCodeArray) in
            self?.log(stdErr: "disableFirewall exit code: \(exitCodeArray[0])")
            completion((exitCodeArray[0] as AnyObject).intValue == 0)
        })
    }
    
    
    func isVPNFirewallEnabled(completion: @escaping (Bool) -> Void) {
        guard let helper = self.helper() else {
            //Log.ET("Can not retrieve network helper", level: .debug)
            log(stdErr: "Can not retrieve network helper")
            completion(false)
            return
        }
        guard let activeEntryIp = lastVpnServerIp else {
            completion(false)
            return
        }
        
        let argumentsArray = ["isVPNFirewallEnabled",activeEntryIp]
        helper.connectWithPaths(pathsData: getDataAug(array: argumentsArray), withArguments: getDataAug(array: argumentsArray), completion:{ [weak self] (exitCodeArray) in
            self?.log(stdErr: "disableFirewall exit code: \(exitCodeArray[0])")
        })
    }
    
    func isAnyFirewallEnabled(completion: @escaping (Bool) -> Void) {
        guard let helper = self.helper() else {
            //Log.ET("Can not retrieve network helper", level: .debug)
            log(stdErr: ("Can not retrieve network helper "))
            return
        }
        
        let argumentsArray = ["isAnyFirewallEnabled"]
        helper.connectWithPaths(pathsData: getDataAug(array: argumentsArray), withArguments: getDataAug(array: argumentsArray), completion:{ [weak self] (exitCodeArray) in
            self?.log(stdErr: "disableFirewall exit code: \(exitCodeArray[0])")
            completion((exitCodeArray[0] as AnyObject).intValue == 0)
        })
    }
    
    private func attemptEnablingFirewall(ipAddress: String, completionHandler: @escaping (Bool) -> ()) throws {
        guard let helper = self.helper() else {
            throw AppError(message: "Can not retrieve network helper") //NSError(code: 0, localizedDescription: "Can not retrieve network helper")
        }
        lastVpnServerIp = ipAddress
        let argumentsArray = ["enableFirewall", getFirewallRules()]
        helper.connectWithPaths(pathsData: getDataAug(array: argumentsArray), withArguments: getDataAug(array: argumentsArray), completion:{ [weak self] (exitCodeArray) in
            if exitCodeArray[0] as! Int == 0 {
                completionHandler(true)
                self?.log(stdErr: "anyFirewallEnabled result: \(exitCodeArray[0])")
//                self?.isAnyFirewallEnabled { (enabled) in
//                    print("FirewallEnabled : \(enabled)")
//                }
            } else {
                completionHandler(false)
                //Log.ET("Helper returned a non-zero exit code: \(exitCode)")
                self?.log(stdErr: "Helper returned a non-zero exit code: \(exitCodeArray[0])")
                self?.isVPNFirewallEnabled { [weak self] (enabled) in
                    if !enabled {
                        //self?.invalidateFirewallTimer()
                        self?.lastVpnServerIp = nil
                    }
                }
            }
        })
        
    }
    
    private func getFirewallRules() -> String {
        return getAllBlockFirewallRules()
    }
    

    private func getAllBlockFirewallRules() -> String {
        let pfConfig = """
               # blockout
               # Block everything leaving our Mac.
               block out all

               # rfc1918
               # Outgoing traffic destined for the local area network.
               table <PrivateIps> const { 192.168.0.0/16 172.16.0.0/12 10.0.0.0/8 }
               pass out on { en0 en1 en2 en3 en4 } to <PrivateIps>

               # dns
               # Outgoing DNS requests, probably needed to resolve your remote VPN server
               pass out on { en0 en1 en2 en3 en4 } proto { udp tcp } from any to any port domain
               pass out on { en0 en1 en2 en3 en4 } proto { udp tcp } from any to any port 64738
               

               # l2tpipsec
               # Establishment of L2TP over IPSec tunnels
               pass out on { en0 en1 en2 en3 en4 } proto udp from any to any port { isakmp l2f ipsec-msft }
               pass out on { en0 en1 en2 en3 en4 } proto esp from any to any

               # vpn
               # Outgoing traffic through VPN tunnels
               pass out on {tun0 ppp0 utun0 utun1 utun2 utun3 utun4 utun5 utun6 utun7 utun8 utun9 utun10 ipsec0} all

               # pptppipsec
               # Establishment of PPTP over IPSec tunnels
               pass out on { en0 en1 en2 en3 en4 } proto tcp from any to any port { pptp }
               pass out on { en0 en1 en2 en3 en4 } proto gre from any to any

               """
        
        return pfConfig
        
    }
    
    // TODO with specific server
    private func getFirewallRulesWithAddress(dnsAddress: String) -> String {
        
        let pfConfig = """
        private_ips = "{ 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16 }"
        vpn_ip = "\(dnsAddress)"
        VPN_PROVIDER_ADDR = "\(dnsAddress)"
        
        # Options
        set block-policy drop
        set fingerprints "/etc/pf.os"
        set ruleset-optimization basic
        set skip on lo0
        
        # Interfaces
        vpn_intf = "{utun0 utun1 utun2 utun3 utun4 utun5 utun6 utun7 utun8 utun9 utun10 ipsec0}"
        
        # Ports
        allowed_vpn_ports = "{1:65535}"
        
        # Table with allowed IPs
        # table <allowed_vpn_ips> persist file "/etc/pf.anchors/vpn.list" file "/etc/pf.anchors/custom.list"
        table <allowed_vpn_ips> {$VPN_PROVIDER_ADDR, $vpn_ip }
        
        # Block all outgoing packets
        block out all
        
        # Antispoof protection
        antispoof for $vpn_intf
        
        # Allow outgoing packets to specified IPs only
        pass out proto icmp from any to <allowed_vpn_ips>
        pass out proto {tcp udp} from any to <allowed_vpn_ips> port $allowed_vpn_ports
        
        # Allow traffic for VPN interfaces
        pass out on $vpn_intf all
        
        """
        
        return pfConfig
    }
        
    private func networkProxyInterfaces() -> [String] {
        guard let cfProxySettings = CFNetworkCopySystemProxySettings()?.takeRetainedValue(),
            let proxySettings = cfProxySettings as? [String: AnyObject],
            let connectedInterfaces = proxySettings["__SCOPED__"] as? [String: AnyObject] else { return [String]() }
        
        // return all running ipsec interface names
        return connectedInterfaces.keys.filter { interface in
            print(interface)
            return true //interface.hasPrefix("ipsec")
        }
    }
    
    private func notifyOfActiveFirewall() {
        isVPNFirewallEnabled { (enabled) in
            if enabled {
                //if enabled && self.appStateManager.state.isSafeToEnd { // prevents quick changes between disconnected and connecting from flashing the popup
            }
        }
    }
    
    // MARK: - Alert inactiveFirewallTimer
    private func startCheckingForInactiveFirewall() {
        inactiveFirewallTimer?.invalidate()
        DispatchQueue.main.async { [unowned self] in
            self.inactiveFirewallTimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.checkForInactiveFirewall), userInfo: nil, repeats: true)
            self.inactiveFirewallTimer?.tolerance = 5
        }
    }
    
    @objc private func checkForInactiveFirewall() {
        isVPNFirewallEnabled { [weak self] (enabled) in
            guard let `self` = self else { return }
            
            if !enabled {
                self.invalidateFirewallTimer()
            }
        }
    }
    
    private func invalidateFirewallTimer() {
        inactiveFirewallTimer?.invalidate()
        inactiveFirewallTimer = nil
    }
    
    private func getDataAug(array: Array<Any>) -> Data {
        do {
            if #available(OSX 10.13, *) {
                return try NSKeyedArchiver.archivedData(withRootObject: array, requiringSecureCoding: false)
            } else {
                return NSKeyedArchiver.archivedData(withRootObject: array);
            }
        } catch {
            return Data()
        }
    }
    
}

// MARK: - AppProtocol functions
extension FirewallManager: AppProtocol {
    public func log(stdOut: String) {
         OperationQueue.main.addOperation {
         //let appDelegate = NSApplication.shared.delegate as! AppDelegate
        guard !stdOut.isEmpty else { return }
        OperationQueue.main.addOperation {
//            print(stdOut)
            //appDelegate.log(stdErr: stdOut)
        }
        }
    }
    
    public func log(stdErr: String) {
         OperationQueue.main.addOperation {
         //let appDelegate = NSApplication.shared.delegate as! AppDelegate
        guard !stdErr.isEmpty else { return }
        OperationQueue.main.addOperation {
//            print(stdErr)
            //appDelegate.log(stdErr: stdErr)
        }
        }
    }
}

// MARK: - Alert functions
extension FirewallManager {
    func showErrorAlert(error: Error) {
        let alert = NSAlert(error: error)
        alert.beginSheetModal(for: NSApp.windows.first!) { (response) in
            if response == .alertFirstButtonReturn {
                
            }
        }
    }
    
    func showAlert(message: String) {
        let alert = NSAlert()
        alert.messageText = message
        
        alert.beginSheetModal(for: NSApp.windows.first!) { (response) in
            if response == .alertFirstButtonReturn {
                
            }
        }
    }
}

// MARK: - Error functions
struct AppError: Error {
    private let message: String
    
    var localizedDescription: String {
        return message
    }
    
    init(message: String) {
        self.message = message
    }
}
