//
//  Helper.swift
//  AtomHelperFramework
//
//  Copyright © 2020 Atom. All rights reserved.
//
//  This file is part of Atom by Secure.

import Foundation
import os.log

extension OSLog {
    private static var subsystem = Bundle.main.bundleIdentifier!

    /// Logs the view cycles like viewDidLoad.
    static let viewCycle = OSLog(subsystem: subsystem, category: "HelperTOOL")
}

class Helper: NSObject, NSXPCListenerDelegate {

    // MARK: -
    // MARK: Private Constant Variables

    private let listener: NSXPCListener

    // MARK: -
    // MARK: Private Variables

    private var connections = [NSXPCConnection]()
    private var shouldQuit = false
    private var shouldQuitCheckInterval = 1.0

    // MARK: -
    // MARK: Initialization

    override init() {
        self.listener = NSXPCListener(machServiceName: HelperConstants.machServiceName)
        super.init()
        self.listener.delegate = self
        print("@Helper Init")
        os_log("@Helper Init", log: OSLog.viewCycle, type: .info)

    }

    public func run() {
        print("@Helper run")
        os_log("@Helper run", log: OSLog.viewCycle, type: .info)

        self.listener.resume()

        // Keep the helper tool running until the variable shouldQuit is set to true.
        // The variable should be changed in the "listener(_ listener:shoudlAcceptNewConnection:)" function.

        while !self.shouldQuit {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: self.shouldQuitCheckInterval))
        }
    }

    // MARK: -
    // MARK: NSXPCListenerDelegate Methods

    func listener(_ listener: NSXPCListener, shouldAcceptNewConnection connection: NSXPCConnection) -> Bool {

        print("@Helper listener(_ listener: NSXPCListener")
        os_log("@Helper listener(_ listener: NSXPCListener", log: OSLog.viewCycle, type: .info)

        // Verify that the calling application is signed using the same code signing certificate as the helper
        guard self.isValid(connection: connection) else {
            print("@Helper self.isValid(connection false")
            os_log("@Helper self.isValid(connection false", log: OSLog.viewCycle, type: .info)

            return false
        }

        print("@Helper self.isValid(connection YES")
        os_log("@Helper self.isValid(connection YES", log: OSLog.viewCycle, type: .info)

        // Set the protocol that the calling application conforms to.
        connection.remoteObjectInterface = NSXPCInterface(with: AppProtocol.self)

        // Set the protocol that the helper conforms to.
        connection.exportedInterface = NSXPCInterface(with: HelperProtocol.self)
        connection.exportedObject = self

        // Set the invalidation handler to remove this connection when it's work is completed.
        connection.invalidationHandler = {
            if let connectionIndex = self.connections.firstIndex(of: connection) {
                self.connections.remove(at: connectionIndex)
            }

            if self.connections.isEmpty {
                self.shouldQuit = true
            }
        }

        self.connections.append(connection)
        connection.resume()

        return true
    }

    // MARK: -
    // MARK: Private Helper Methods

    private func isValid(connection: NSXPCConnection) -> Bool {
        do {
            return try CodesignCheck.codeSigningMatches(pid: connection.processIdentifier)
        } catch {
            os_log("@Helper Code signing check failed with error", log: OSLog.viewCycle, type: .info)

            print("Code signing check failed with error")
            NSLog("Code signing check failed with error: \(error)")
            return false
        }
    }

    private func verifyAuthorization(_ authData: NSData?, forCommand command: Selector) -> Bool {
        print("@Helper verifyAuthorization")
        do {
            try HelperAuthorization.verifyAuthorization(authData, forCommand: command)
        } catch {
            if let remoteObject = self.connection()?.remoteObjectProxy as? AppProtocol {
                remoteObject.log(stdErr: "Authentication Error: \(error)")
            }
            return false
        }
        return true
    }

    private func connection() -> NSXPCConnection? {
        return self.connections.last
    }

    private func runTask(command: String, arguments: Array<String>, completion:@escaping ((NSNumber) -> Void)) -> Void {
        let task = Process()
        let stdOut = Pipe()

        let stdOutHandler =  { (file: FileHandle!) -> Void in
            let data = file.availableData
            guard let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return }
            if let remoteObject = self.connection()?.remoteObjectProxy as? AppProtocol {
                remoteObject.log(stdOut: output as String)
            }
        }
        stdOut.fileHandleForReading.readabilityHandler = stdOutHandler

        let stdErr:Pipe = Pipe()
        let stdErrHandler =  { (file: FileHandle!) -> Void in
            let data = file.availableData
            guard let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return }
            if let remoteObject = self.connection()?.remoteObjectProxy as? AppProtocol {
                remoteObject.log(stdErr: output as String)
            }
        }
        stdErr.fileHandleForReading.readabilityHandler = stdErrHandler

        task.launchPath = command
        task.arguments = arguments
        task.standardOutput = stdOut
        task.standardError = stdErr

        task.terminationHandler = { task in
            completion(NSNumber(value: task.terminationStatus))
        }

        task.launch()
    }
    
    private func remoteObject() -> AppProtocol? {
           return connection()?.remoteObjectProxy as? AppProtocol
       }
}

extension Helper: HelperProtocol{
    
    // MARK: -
       // MARK: HelperProtocol Methods

       func runCommandLs(withPath path: String, completion: @escaping (NSNumber) -> Void) {

           // For security reasons, all commands should be hardcoded in the helper
           let command = "/bin/ls"
           let arguments = [path]

           // Run the task
           self.runTask(command: command, arguments: arguments, completion: completion)
       }

       func runCommandLs(withPath path: String, authData: NSData?, completion: @escaping (NSNumber) -> Void) {

           // Check the passed authorization, if the user need to authenticate to use this command the user might be prompted depending on the settings and/or cached authentication.

//           guard self.verifyAuthorization(authData, forCommand: #selector(HelperProtocol.runCommandLs(withPath:authData:completion:))) else {
//               completion(kAuthorizationFailedExitCode)
//               return
//           }

           self.runCommandLs(withPath: path, completion: completion)
       }
    
    func getVersion(completion: @escaping (String) -> ()) {
        guard let logger = remoteObject() else {
            completion("-10")
            return
        }
        logger.log(stdErr: "connectWithPaths - - - CFBundleShortVersionString " )

        completion(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "0")
    }
    
    
    func unload(completion: @escaping (NSNumber) -> ()) {
        CommandLineToolRunner.unloadFromLaunchd(completion: completion)
    }
    
    func anyFirewallEnabled(completion: @escaping (NSNumber) -> Void) {
        guard let logger = remoteObject() else {
            completion(-1)
            return
        }
        logger.log(stdErr: "anyFirewallEnabled")
        CommandLineToolRunner.checkIfAnyFirewallIsEnabled(logger: logger, completion: completion)
    }
    
    func firewallEnabled(forServer address: String, completion: @escaping (NSNumber) -> ()) {
        guard let logger = remoteObject() else {
            completion(-1)
            return
        }
         logger.log(stdErr: "firewallEnabled")
        CommandLineToolRunner.checkIfFirewallIsEnabled(forServer: address, logger: logger, completion: completion)
    }
    
    func enableFirewall(with config: String, completion: @escaping (NSNumber) -> ()) {
        guard let logger = remoteObject() else {
                   completion(-1)
                   return
               }
         logger.log(stdErr: "enableFirewall")
        CommandLineToolRunner.enableFirewall(with: config, logger: logger, completion: completion)
    }
    
    func disableFirewall(completion: @escaping (NSNumber) -> ()) {
        CommandLineToolRunner.disableFirewall(completion: completion)
    }
    
    func connectWithPaths(pathsData: Data, withArguments augsData: Data, completion: @escaping (Array<Any>) -> ())
    {
        do {
            if let augsData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(augsData) as? [String] {
                
                guard let logger = remoteObject() else {
                           completion([-1])
                           return
                       }
                logger.log(stdErr: "connectWithPaths - - - "  + augsData[0])
                
                if (augsData[0] == "getVersion") {
                    getVersion() { (exitCode) in
                        completion([exitCode])
                    }
                }else if (augsData[0] == "enableFirewall") {
                    enableFirewall(with: augsData[1]) { (exitCode) in
                        completion([exitCode])
                    }
                }else if (augsData[0] == "disableFirewall") {
                    disableFirewall(){ (exitCode) in
                        completion([exitCode])
                    }
                }else if (augsData[0] == "isVPNFirewallEnabled") {
                    firewallEnabled(forServer: augsData[1]){ (exitCode) in
                        completion([exitCode])
                    }
                }else if (augsData[0] == "isAnyFirewallEnabled") {
                    anyFirewallEnabled(){ (exitCode) in
                        completion([exitCode])
                    }
                }else if (augsData[0] == "unloadAndInstallHelper") {
                    unload(){ (exitCode) in
                        completion([exitCode])
                    }
                }
            }
        } catch {
            print("Couldn't read file.")
        }
        
    }
}
