//
//  HelperConstants.swift
//  AtomHelperFramework
//
//  Copyright © 2020 Atom. All rights reserved.
//
//  This file is part of Atom by Secure.
//  Don't change this file, its been automatically update when installation script runs.

import Foundation

struct HelperConstants {
static var machServiceName = "com.atom.helpertool"
}
