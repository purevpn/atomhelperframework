//
//  HelperProtocol.swift
//  AtomHelperFramework
//
//  Copyright © 2020 Atom. All rights reserved.
//
//  This file is part of Atom by Secure.

import Foundation

@objc(HelperProtocol)
public protocol HelperProtocol {
    //func getVersion(completion: @escaping (String) -> Void)
    func connectWithPaths(pathsData: Data, withArguments augsData: Data, completion: @escaping (Array<Any>) -> ())
}
