//
//  CommandLineToolRunner.swift
//  AtomHelperFramework
//
//  Copyright © 2020 Atom. All rights reserved.
//
//  This file is part of Atom by Secure.
//

import Foundation

struct CommandLineToolRunner {
    
    private static let pfctl = "/sbin/pfctl"
    private static let undoArguments = ["-d"]
    private static let deleteAllRulesArguments = ["-Fa"]
    private static let stateArguments = ["-si"]
    private static let rulesArguments = ["-sr"]
    
    private static let grep = "/usr/bin/grep"
    private static let grepEnabledArguments = ["Status: Enabled"]
    
    private static let launchctl = "/bin/launchctl"
    private static let unloadArguments = ["unload", "/Library/LaunchDaemons/\(HelperConstants.machServiceName).plist"]
    
    private static let PFAnchorPath = "/etc/pf.anchors/com.iks.rules."

    /// Used to run commands in serial
    private static let runnerQueue = DispatchQueue(label: "com.atom.Helpertool")
    
    /// Check if any PF firewall is enabled (regardless of rules)
    static func checkIfAnyFirewallIsEnabled(logger: AppProtocol, completion: @escaping (NSNumber) -> ()) {
        checkPfEnabled(logger: logger) { (task) in
            let terminationStatus = NSNumber(value: task.terminationStatus)
            logger.log(stdErr: "Any enabled result: \(terminationStatus)\n")
            completion(terminationStatus)
        }
    }
    
    /// Check if our firewall is enabled
    static func checkIfFirewallIsEnabled(forServer address: String, logger: AppProtocol, completion: @escaping (NSNumber) -> ()) {
        checkPfEnabled(logger: logger) { (task) in
            let terminationStatus = NSNumber(value: task.terminationStatus)
            var loggingOutput = "Enabled result: \(terminationStatus)\n"
            guard terminationStatus == 0 else {
                logger.log(stdErr:loggingOutput)
                completion(terminationStatus)
                return
            }
            
            // Once the firewall is confirmed to be enabled, check the firewall rules
            // pfctl -sr | grep <address>
            let grepAdressInputPipe = Pipe()
            
            let rulesOutputHandler =  { (file: FileHandle!) -> Void in
                let data = file.availableData
                grepAdressInputPipe.fileHandleForWriting.write(data)
                grepAdressInputPipe.fileHandleForWriting.closeFile()
                
                guard let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue), output.length != 0 else { return }
                loggingOutput.append(output as String)
            }
            
            let firewallRulesPipe = Pipe()
            firewallRulesPipe.fileHandleForReading.readabilityHandler = rulesOutputHandler
            
            let pfctlRulesProcess = Process()
            pfctlRulesProcess.launchPath = self.pfctl
            pfctlRulesProcess.arguments = self.rulesArguments
            pfctlRulesProcess.standardOutput = firewallRulesPipe
            
            let grepAddressProcess = Process()
            grepAddressProcess.launchPath = self.grep
            grepAddressProcess.arguments = [address]
            grepAddressProcess.standardInput = grepAdressInputPipe
            
            grepAddressProcess.terminationHandler = { task in
                let terminationStatus = NSNumber(value: task.terminationStatus)
                
                if terminationStatus != 0 {
                    loggingOutput.append("Address present result: \(terminationStatus)")
                    logger.log(stdErr:loggingOutput)
                }
                
                completion(terminationStatus)
            }
            
            pfctlRulesProcess.launch()
            pfctlRulesProcess.waitUntilExit()
            grepAddressProcess.launch()
            grepAddressProcess.waitUntilExit()
        }
    }
    
    static func enableFirewall(with pfConfig: String, logger: AppProtocol, completion: @escaping (NSNumber) -> ()) {
        
        do {
            try writeFirewallRules(pfConfig: pfConfig)
            } catch {
                completion(-1)
        }
        
        logger.log(stdErr: "- - - writeFirewallRules - - - ")
        
        runPfctl(arguments: undoArguments) { _ in
            let arguments = self.applyArguments(file: URL(string: CommandLineToolRunner.PFAnchorPath + getBundleIdentifier())!)
            self.runPfctl(arguments: arguments, completion: completion)
        }
    }
    
    static func disableFirewall(completion: @escaping (NSNumber) -> ()) {
//        do {
//            try deleteFile()
//        } catch {
//        }
        runPfctl(arguments: undoArguments, completion: completion)
    }
    
    static func deleteFile() throws {
        try FileManager.default.removeItem(atPath: (CommandLineToolRunner.PFAnchorPath + getBundleIdentifier()))
    }
    
    static func getBundleIdentifier() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as? String ?? "0"
    }
    
    static private func writeFirewallRules(pfConfig : String) throws {
        try pfConfig.write(toFile: (CommandLineToolRunner.PFAnchorPath + getBundleIdentifier()), atomically: true, encoding: .utf8)
    }
    
    static func unloadFromLaunchd(completion: @escaping (NSNumber) -> ()) {
        runnerQueue.async {
            let process = Process()
            process.launchPath = launchctl
            process.arguments = unloadArguments
            
            process.terminationHandler = { task in
                completion(NSNumber(value: task.terminationStatus))
            }
            
            process.launch()
        }
    }
    
    // MARK:- Private functions
    private static func applyArguments(file: URL) -> [String] {
        let configFile = file.path
        return ["-Fa", "-f", configFile, "-e"]
    }
    
    private static func checkPfEnabled(logger: AppProtocol, completion: @escaping (Process) -> Void) {
        runnerQueue.async {
            let grepEnabledInputPipe = Pipe()
            
            let statusOutputHandler =  { (file: FileHandle!) -> Void in
                let data = file.availableData
                grepEnabledInputPipe.fileHandleForWriting.write(data)
                grepEnabledInputPipe.fileHandleForWriting.closeFile()
                
                guard let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue), output.length != 0 else { return }
                logger.log(stdErr:output as String)
                print(output as String)

            }
            
            // First check that the firewall is enabled
            // pfctl -si | grep 'Status: Enabled'
            let firewallStatusPipe = Pipe()
            firewallStatusPipe.fileHandleForReading.readabilityHandler = statusOutputHandler
            
            let pfctlStatusProcess = Process()
            pfctlStatusProcess.launchPath = pfctl
            pfctlStatusProcess.arguments = stateArguments
            pfctlStatusProcess.standardOutput = firewallStatusPipe
            
            let grepEnabledProcess = Process()
            grepEnabledProcess.launchPath = grep
            grepEnabledProcess.arguments = grepEnabledArguments
            grepEnabledProcess.standardInput = grepEnabledInputPipe
            
            grepEnabledProcess.terminationHandler = { task in
                completion(task)
            }
                
            pfctlStatusProcess.launch()
            pfctlStatusProcess.waitUntilExit()
            grepEnabledProcess.launch()
            grepEnabledProcess.waitUntilExit()
        }
    }
    
    private static func runPfctl(arguments: [String], completion: @escaping ((NSNumber) -> ())) {
        runnerQueue.async {
            let process = Process()
            process.launchPath = pfctl
            process.arguments = arguments
            
            process.terminationHandler = { task in
                completion(NSNumber(value: task.terminationStatus))
            }
            
            process.launch()
        }
    }
}
