# AtomHelperFramework

AtomHelperFramework is a framework that handles installation of privileged helper tool and manage advance features like

  - Enabling Internet Kill Switch
  - Disabling Internet Kill Switch
  - Checking the Status of Internet Kill Switch

## Installation

### Step # 1
Install framework as submodule in your project directory.

```sh
$ git submodule add git@bitbucket.org:purevpn/atomhelperframework.git Submodule/AtomHelperFramework
```

If want to update the submodule

```sh
$ git submodule foreach git pull
```

### Step # 2

Integrate submodule target in App's Project

![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/Submodule.png = 250x)


### Step # 3

Add AtomHelperFramework as dependency in build phase of your application.

![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/dependency-1.png)
![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/dependency-2.png)

Also add it in Frameworks,Libraries and Embedded Contents by pressing Plus (+) Sign as shown in image below.

![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/embeddedframework-1.png)

### Step # 4

Add Copy Files from Build Phases as shown in image.    
Destination : Wrapper     
Subpath     : Contents/Library/LaunchServices     
Code sign on Copy : Checked     

![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/copyfiles.png)

![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/copyfiles-2.png)

![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/copyfiles-3.png)

### Step # 5

Add Run Script in build phases of your Application Target. 

```sh
"${SRCROOT}"/Submodule/AtomHelperFramework/Scripts/CodeSignUpdate.sh
```
#### Note : Make sure that the path is correct.

![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/runscript-1.png)


 
### Step # 6

Its time to run script written on terminal. This script will rename the target for your helpertool. 

```sh
$ cd Submodule/AtomHelperFramework/
$ sudo ruby install_script.rb
```

This script will ask for the following two information   
1. Your Application Bundle Identifier    
2. Your desired HelperTool Identifier   

![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/installationscript-1.png)
![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/installationscript-2.png)
![](https://bitbucket.org/purevpn/atomhelperframework/raw/73ee664ead9553903ca7f085a8ad10493acc2f38/Images/installationscript-3.png)

### Step # 7

Code signing is very important. Your application, AtomHelperFramework and HelperTool target must have Mac Developer certificate or Apple development selected instead of Automatic.
Please use SMJobBlessUtil for codesign probided in scripts folder using the following command 

```sh
$ python SMJobBlessUtil.py check path/of/your/App.app
``` 

If everything is set perfectly it will not log anything on terminal otherwise will point out the error.


### Step # 8

1. Clean your project 
2. Build your project

Once the installation is completed, build your application. Now when you search the helpertool identifier provided above in your entire project, it should be in 8 different places as shown below. 



### Check Status of Helper Tool

In order to check the status of Helper Tool use the following code snippet as shown in demo application.
```sh
        // Check if the current embedded helper tool is installed on the machine.
        FirewallManager.shared.helperInstallStatus { installed in
            OperationQueue.main.addOperation {
            }
        }
```
### Install Helper Tool

In order to install Helper Tool use the following code snippet as shown in demo application.

```sh
        // install helper tool on machine.
        FirewallManager.shared.helperInstall()
```
### Enable Internet Kill Switch

In order to enable Internet Kill Switch use the following code snippet.

```sh
        FirewallManager.shared.enableFirewall { (value) in
        }
```

### Disable Internet Kill Switch

In order to disable Internet Kill Switch use the following code snippet.

```sh
        FirewallManager.shared.disableFirewall { (value) in
        }
```

### Check Status of Internet Kill Switch

In order to check Internet Kill Switch use the following code snippet.

```sh
        FirewallManager.shared.isAnyFirewallEnabled { (status) in
            OperationQueue.main.addOperation {
            }
        }
```

# Demo Application

Demo Application can be clone from https://bitbucket.org/purevpn/atomhelperframeworkdemo/


